<?php

/*
|--------------------------------------------------------------------------
| Пример файла с конфигами.
| Необходимо переименовать в config.php
|--------------------------------------------------------------------------
|
| token - authentication token бота, https://core.telegram.org/bots/api#authorizing-your-bot
|
*/

return $config = [
    'token' => '123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11',
];