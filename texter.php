<?php

class Texter {

    private $welcome    = ['привет', 'дратути', 'здравствтуйте', 'здарова', 'салам'];
    private $dvizh      = ['движ'];
    private $pass       = ['пас', '-'];
    private $daur       = ['даур', '@TedD1'];
    private $joke       = ['\sшч', '^шч$', 'шут*', 'шуч*'];

    private $answers    = [
            'welcome' => "привет @",
            'dvizh'   => "движ а у",
            'daur'    => "Слава великому Дауру!",
            'joke'    => "ха ха",
    ];


    public function __construct()
    {
        $today = date('N');

        if ($today == 4) {

            $this->answers = [
                'welcome' => "hello @",
                'dvizh'   => "party!!!",
                'daur'    => "Daur the greatest!",
                'joke'    => "ha ha",
            ];

            $this->welcome  = ['hello'];
            $this->dvizh    = ['движ', 'dvizh'];
            $this->daur     = ['даур', 'daur', '@TedD1'];
            $this->joke     = ['joke'];
        }
    }

    /* ответ на вопрос */

    public function answer($question)
    {
        return isset($this->answers[$question]) ? $this->answers[$question] : "";
    }

    /** проверка на 'привет', 'дратути', 'здравствтуйте', 'здарова', 'салам' */

    public function welcome($message)
    {
        $text = mb_strtolower($message);

        $dict = $this->welcome;

        $validation = false;

        foreach ($dict as $word) {
            $pattern = "/" . $word . "*/i";
            if (preg_match($pattern, $text) != false) {
                $validation = true;
                break;
            }
        }

        return $validation;
    }

    public function dvizh($message)
    {
        $text = mb_strtolower($message);

        $dict = $this->dvizh;

        $validation = false;

        foreach ($dict as $word) {
            $pattern = "/" . $word . "/i";
            if (preg_match($pattern, $text) != false) {
                $validation = true;
                break;
            }
        }

        return $validation;
    }

    public function daur($message)
    {
        $text = mb_strtolower($message);

        $dict = $this->daur;

        $validation = false;

        foreach ($dict as $word) {
            $pattern = "/" . $word . "*/i";
            if (preg_match($pattern, $text) != false) {
                $validation = true;
                break;
            }
        }

        return $validation;
    }

    public function joke($message)
    {
        $text = mb_strtolower($message);

        $dict = $this->joke;

        $validation = false;

        foreach ($dict as $word) {
            $pattern = "/" . $word . "/i";
            if (preg_match($pattern, $text) != false) {
                $validation = true;
                break;
            }
        }

        return $validation;
    }

    public function pass($message)
    {
        $text = mb_strtolower($message);

        $dict = $this->pass;

        $validation = false;

        foreach ($dict as $word) {
            $pattern = "/\b" . $word . "\b/gi";
            if (preg_match($pattern, $text) != false) {
                $validation = true;
                break;
            }
        }

        return $validation;
    }

}