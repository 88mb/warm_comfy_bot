<?php

function file_save($filename, $data, $mode = 'a')
    {

        $fp = fopen($filename, $mode);
        $retries        = 0;
        $max_retries    = 100;

        if (! $fp)
        {
          // failure
          return false;
        }

        // keep trying to get a lock as long as possible
        do
        {
          if ($retries > 0)
          {
            usleep(rand(1, 10000));
          }
          $retries += 1;
        }
        while (! flock($fp, LOCK_EX) and $retries <= $max_retries);

        // couldn't get the lock, give up
        if ($retries == $max_retries)
        {
          // failure
          return false;
        }

        // got the lock, write the data
        fwrite($fp, "$data" . PHP_EOL);

        // release the lock
        flock($fp, LOCK_UN);
        fclose($fp);

        // success
        return true;
    }