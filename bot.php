<?php

class Bot {

    private $url;

    public function __construct($token)
    {
        $url    = "https://api.telegram.org/";
        $url   .= "bot" . $token . "/";

        $this->url = $url;
    }

    private function execute($url)
    {
        // создание нового ресурса cURL
        $ch = curl_init();

        // установка URL и других необходимых параметров
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // загрузка страницы и выдача её браузеру
        curl_exec($ch);

        // завершение сеанса и освобождение ресурсов
        curl_close($ch);

        return true;
    }

    public function sendMessage($chat, $message)
    {
        $url    = $this->url;
        $url   .= "sendmessage";
        $url   .= "?chat_id=" . $chat;
        $url   .= "&text=" . urlencode($message);
        $url   .= "&disable_notification=true";

        $this->execute($url);

        return true;
    }

    public function replyMessage($chat, $replyMessageId, $message)
    {
        $url    = $this->url;
        $url   .= "sendmessage";
        $url   .= "?chat_id=" . $chat;
        $url   .= "&reply_to_message_id=" . $replyMessageId;
        $url   .= "&text=" . urlencode($message);
        $url   .= "&disable_notification=true";

        $this->execute($url);

        return true;
    }

}