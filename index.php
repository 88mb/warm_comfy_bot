<?php

require_once "config.php";
require_once "logger.php";
require_once "bot.php";
require_once "texter.php";

// Входные комманды
$input = file_get_contents('php://input');

// Логирование входящих комманд по веб-хуку
// Необходимо создать файл input.txt или закоментить

file_save('input.txt', $input);


// Инициализация Бота
$token  = $config['token'];
$bot    = new Bot($token);

// Инициалтзация анализатора текста
$texter         = new Texter();

$update         = json_decode($input);

$chat           = $update->message->chat->id;
$username       = $update->message->from->username;
$messageId      = $update->message->message_id;
$text           = $update->message->text;


if ($texter->welcome($text) == true) {
    $bot->sendMessage($chat, $texter->answer('welcome') . $username);
}

if ($texter->dvizh($text) == true) {
    $bot->sendMessage($chat, $texter->answer('dvizh'));
}

if ($texter->daur($text) == true) {
    $bot->sendMessage($chat, $texter->answer('daur'));
}

if ($texter->joke($text) == true) {
    $bot->replyMessage($chat, $messageId, $texter->answer('joke'));
}

/*if ($texter->pass($text) == true) {
    $bot->replyMessage($chat, $messageId, "котакпас");
}*/


